import 'dart:io';
import 'dart:math';

void main() {
  final testCount = int.parse(stdin.readLineSync());
  for (int line = 1; line <= testCount; line++) {
    final secs = minSecondsBeforeWar(stdin.readLineSync());
    stdout.writeln('Case #${line}: ${secs}');
  }
}

int minSecondsBeforeWar(String input) {
  // To minimize seconds before ware, choose the smallest possible base
  // The base can't be smaller than 2.
  final minBase = max(uniqueCharCount(input), 2);
  // Use the smallest possible digit for the highest powers.
  // But we can't use 0, so we'll use 1 for the first character instead.
  var minSecs = 1 * pow(minBase, input.length - 1);
  final digits = { input[0]: 1};

  int lowestAvailableDigit = 0;
  int currentPower = input.length - 2;
  for (int i = 1; i < input.length; i++) {
    if (digits[input[i]]==null) {
      digits[input[i]] = lowestAvailableDigit;
      if( lowestAvailableDigit == 0 ) {
        lowestAvailableDigit = 2;
      } else {
        lowestAvailableDigit++;
      }
    }
    final currentDigit = digits[input[i]];
    minSecs += currentDigit * pow(minBase, currentPower);
    currentPower--;
  }
  return minSecs;
}

int uniqueCharCount(String input) {
  final chars = new Set();

  for (int i = 0; i < input.length; i++) {
    chars.add(input[i]);
  }
  return chars.length;
}